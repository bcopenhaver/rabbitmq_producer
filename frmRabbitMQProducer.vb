﻿Imports RabbitMQ.Client
Imports System.Text

Public Class frmRabbitMQProducer

    'WORK QUEUE
    'Const QUEUE_NAME As String = "turning-work-queue"

    'PUB/SUB
    Const EXCHANGE_NAME As String = "turning-pubsub-exchange"

    'ROUTING
    Const ROUTING_EXCHANGE_NAME As String = "turning-routing-exchange"
    Const ROUTING_KEY As String = "turning-test-routing"

    Private connectionFactory As New ConnectionFactory
    Private connection As IConnection
    Private channel As IModel

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub frmRabbitMQProducer_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lblIP.Text = GetIPAddress()
        lblHost.Text = System.Net.Dns.GetHostName

        connectionFactory.UserName = "turning"
        connectionFactory.Password = "ttech"
        connection = connectionFactory.CreateConnection
        channel = connection.CreateModel()

        ''WORK QUEUE
        'channel.QueueDeclare(QUEUE_NAME, False, False, False, Nothing)

        'PUB/SUB
        channel.ExchangeDeclare(EXCHANGE_NAME, ExchangeType.Fanout)

        'ROUTING
        channel.ExchangeDeclare(ROUTING_EXCHANGE_NAME, ExchangeType.Direct)
        'channel.QueueBind(QUEUE_NAME, ROUTING_EXCHANGE_NAME, ROUTING_KEY)
    End Sub

    Private Sub frmRabbitMQProducer_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        channel.Close()
        connection.Close()
    End Sub

    Private Sub btnPublish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPublish.Click
        Dim message As Byte()
        Dim props As IBasicProperties

        props = channel.CreateBasicProperties()
        props.DeliveryMode = 2
        props.SetPersistent(True)

        pbMessageProgress.Value = 1
        pbMessageProgress.Maximum = nudNumberOfMessages.Value

        For i As Integer = 1 To nudNumberOfMessages.Value
            message = Encoding.UTF8.GetBytes("Test Message " & i.ToString())

            ''WORK QUEUE
            'channel.BasicPublish(String.Empty, QUEUE_NAME, props, message)

            'PUB/SUB
            channel.BasicPublish(EXCHANGE_NAME, String.Empty, props, message)

            lblNumber.Text = i.ToString & " of " & nudNumberOfMessages.Value
            pbMessageProgress.Value = i
            Application.DoEvents()
        Next

        'ROUTING
        message = Encoding.UTF8.GetBytes("Direct Message")
        channel.BasicPublish(ROUTING_EXCHANGE_NAME, ROUTING_KEY, False, props, message)
        
    End Sub

    Private Sub nudNumberOfMessages_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles nudNumberOfMessages.LostFocus
        lblNumber.Text = "0 of " & nudNumberOfMessages.Value
        pbMessageProgress.Value = 0
    End Sub

    Private Sub nudNumberOfMessages_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles nudNumberOfMessages.ValueChanged
        lblNumber.Text = "0 of " & nudNumberOfMessages.Value
        pbMessageProgress.Value = 0
    End Sub

    Private Function GetIPAddress() As String
        Dim nicList() As System.Net.NetworkInformation.NetworkInterface = System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces
        Dim nic As System.Net.NetworkInformation.NetworkInterface
        Dim address As System.Net.NetworkInformation.UnicastIPAddressInformation
        For Each nic In nicList
            If Not nic.GetIPProperties.GetIPv4Properties Is Nothing And nic.GetIPProperties.GatewayAddresses.Count > 0 AndAlso Not nic.GetIPProperties.GatewayAddresses(0).Address.ToString = "0.0.0.0" Then
                For Each address In nic.GetIPProperties.UnicastAddresses
                    If address.Address.AddressFamily = System.Net.Sockets.AddressFamily.InterNetwork Then
                        Return address.Address.ToString
                    End If
                Next
            End If
        Next
        Return ""
    End Function

End Class
