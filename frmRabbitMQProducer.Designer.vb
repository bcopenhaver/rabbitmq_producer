﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRabbitMQProducer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRabbitMQProducer))
        Me.btnPublish = New System.Windows.Forms.Button()
        Me.lblNumberOfMessages = New System.Windows.Forms.Label()
        Me.nudNumberOfMessages = New System.Windows.Forms.NumericUpDown()
        Me.pbMessageProgress = New System.Windows.Forms.ProgressBar()
        Me.lblNumberSent = New System.Windows.Forms.Label()
        Me.lblNumber = New System.Windows.Forms.Label()
        Me.lblIPAddress = New System.Windows.Forms.Label()
        Me.lblIP = New System.Windows.Forms.Label()
        Me.lblHostName = New System.Windows.Forms.Label()
        Me.lblHost = New System.Windows.Forms.Label()
        CType(Me.nudNumberOfMessages, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnPublish
        '
        resources.ApplyResources(Me.btnPublish, "btnPublish")
        Me.btnPublish.Name = "btnPublish"
        Me.btnPublish.UseVisualStyleBackColor = True
        '
        'lblNumberOfMessages
        '
        resources.ApplyResources(Me.lblNumberOfMessages, "lblNumberOfMessages")
        Me.lblNumberOfMessages.Name = "lblNumberOfMessages"
        '
        'nudNumberOfMessages
        '
        resources.ApplyResources(Me.nudNumberOfMessages, "nudNumberOfMessages")
        Me.nudNumberOfMessages.Maximum = New Decimal(New Integer() {100000, 0, 0, 0})
        Me.nudNumberOfMessages.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudNumberOfMessages.Name = "nudNumberOfMessages"
        Me.nudNumberOfMessages.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'pbMessageProgress
        '
        resources.ApplyResources(Me.pbMessageProgress, "pbMessageProgress")
        Me.pbMessageProgress.Name = "pbMessageProgress"
        '
        'lblNumberSent
        '
        resources.ApplyResources(Me.lblNumberSent, "lblNumberSent")
        Me.lblNumberSent.Name = "lblNumberSent"
        '
        'lblNumber
        '
        resources.ApplyResources(Me.lblNumber, "lblNumber")
        Me.lblNumber.Name = "lblNumber"
        '
        'lblIPAddress
        '
        resources.ApplyResources(Me.lblIPAddress, "lblIPAddress")
        Me.lblIPAddress.Name = "lblIPAddress"
        '
        'lblIP
        '
        resources.ApplyResources(Me.lblIP, "lblIP")
        Me.lblIP.Name = "lblIP"
        '
        'lblHostName
        '
        resources.ApplyResources(Me.lblHostName, "lblHostName")
        Me.lblHostName.Name = "lblHostName"
        '
        'lblHost
        '
        resources.ApplyResources(Me.lblHost, "lblHost")
        Me.lblHost.Name = "lblHost"
        '
        'frmRabbitMQProducer
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.Controls.Add(Me.lblHost)
        Me.Controls.Add(Me.lblHostName)
        Me.Controls.Add(Me.lblIP)
        Me.Controls.Add(Me.lblIPAddress)
        Me.Controls.Add(Me.lblNumber)
        Me.Controls.Add(Me.lblNumberSent)
        Me.Controls.Add(Me.pbMessageProgress)
        Me.Controls.Add(Me.nudNumberOfMessages)
        Me.Controls.Add(Me.lblNumberOfMessages)
        Me.Controls.Add(Me.btnPublish)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmRabbitMQProducer"
        CType(Me.nudNumberOfMessages, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnPublish As System.Windows.Forms.Button
    Friend WithEvents lblNumberOfMessages As System.Windows.Forms.Label
    Friend WithEvents nudNumberOfMessages As System.Windows.Forms.NumericUpDown
    Friend WithEvents pbMessageProgress As System.Windows.Forms.ProgressBar
    Friend WithEvents lblNumberSent As System.Windows.Forms.Label
    Friend WithEvents lblNumber As System.Windows.Forms.Label
    Friend WithEvents lblIPAddress As System.Windows.Forms.Label
    Friend WithEvents lblIP As System.Windows.Forms.Label
    Friend WithEvents lblHostName As System.Windows.Forms.Label
    Friend WithEvents lblHost As System.Windows.Forms.Label

End Class
